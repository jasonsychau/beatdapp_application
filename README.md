# beatdapp_application

To run,

- open a terminal window
- cd to ./beatdapp_server
- npm start
- open another terminal window
- cd to ./beatdapp
- npm start (and reply Y to another port)
- open browser to localhost:XXXXX (where XXXXX is the new port - probably 3001)
