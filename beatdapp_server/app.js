var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var bodyParser = require('body-parser');
var cors = require('cors');

var recordRouter = require('./routes/record');
var recordsRouter = require('./routes/records');
var categoriesRouter = require('./routes/categories');

var app = express();

app.set('port', 3000);

app.use(logger('dev'));
app.use(express.json());


app.use(bodyParser.json());
app.use(cors());

app.use('/record', recordRouter);
app.use('/categories', categoriesRouter);
app.use('/records', recordsRouter);

app.use(cookieParser());

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

app.route

module.exports = app;
