var express = require('express');
var router = express.Router();
const sqlite3 = require('sqlite3').verbose();

/* GET home page. */
router.post('/', function(req, res, next) {
	function send(object) {
		res.send(JSON.stringify(object));
	}

	let db = new sqlite3.Database('./sample.db', sqlite3.OPEN_READWRITE, (err) => {
		if (err) {
			send({ error: 'Sorry, no database connection made: '+err.toString() });
		} else {
			db.serialize(() => {
				db.run('INSERT INTO Record VALUES ((SELECT IFNULL(MAX(id),0)+1 FROM Record), "'+req.body.name+'", '+req.body.cost+','+req.body.category+');')
					.all('SELECT id, name, cost FROM Record;', 	(err, rows) => {
						db.close();
						if (err) {
							send({ error: 'Sorry, we could not query database: '+err.toString() });	
						} else {
							send(rows);
						}
					})
			});
		}
	});
});

router.delete('/', function(req, res, next) {
	function send(object) {
		res.send(JSON.stringify(object));
	}

	let db = new sqlite3.Database('./sample.db', sqlite3.OPEN_READWRITE, (err) => {
		if (err) {
			send({ error: 'Sorry, no database connection made: '+err.toString() });
		} else {
			db.serialize(() => {
				db.run('DELETE FROM Record WHERE id = '+req.body.id+';')
					.all('SELECT Record.id, Record.name as name, cost, Category.name as category FROM Record inner INNER JOIN Category ON Record.category = Category.id;', 	(err, rows) => {
						db.close();
						if (err) {
							send({ error: 'Sorry, we could not query database: '+err.toString() });	
						} else {
							send(rows);
						}
					})
			});
		}
	});
});

module.exports = router;
