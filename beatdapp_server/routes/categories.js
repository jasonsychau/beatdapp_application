var express = require('express');
var router = express.Router();
const sqlite3 = require('sqlite3').verbose();


router.get('/', function(req, res, next) {
	function send(object) {
		res.send(JSON.stringify(object));
	}

	let db = new sqlite3.Database('./sample.db', sqlite3.OPEN_READONLY, (err) => {
		if (err) {
			send({ error: 'Sorry, no database connection made: '+err.toString() });
		} else {
			db.all('SELECT id, name FROM Category;', [], (err, rows) => {
				db.close();
				if (err) {
					send({ error: 'Sorry, we could not query database: '+err.toString() });
				} else {
					send(rows);
				}
			});
		}
	});
});

module.exports = router;
