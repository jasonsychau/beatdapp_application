CREATE TABLE Category (id INTEGER PRIMARY KEY, name TEXT NOT NULL);
INSERT INTO Category VALUES (0, "Sports"), (1, "Electronics"), (3, "Clothing");

CREATE TABLE Record (id INTEGER PRIMARY KEY, name TEXT NOT NULL, cost INTEGER NOT NULL, category INTEGER NOT NULL, FOREIGN KEY(category) REFERENCES Category(id));