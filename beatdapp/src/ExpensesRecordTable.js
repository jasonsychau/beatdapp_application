import React from 'react'

class ExpensesRecordTable extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			records: [],
			loading: false,
			errorMessage: ''
		};

	}
	fetchRecords() {
		this.setState({
			...this.state,
			loading: true,
			errorMessage: ''
		}, () => {
			fetch('http://localhost:3000/records').then((response) => {
				if (response.status >= 400) {
                    return {error: 'Sorry, we received a bad server response code: '+String(response.status)+' - '+response.statusText};
                } else {
                    return response.json();
                }
			}).then((json) => {
				if (Object.keys(json).indexOf('error') === -1) {
					this.setState({
						records: json,
						loading: false,
						errorMessage: ''
					});
				} else {
					this.setState({
						...this.state,
						errorMessage: json.error
					});
				}
			}).catch((err) => {
				this.setState({
					...this.state,
					errorMessage: 'Sorry, we found a request error.'
				})
			})
		});
	}
	deleteRecord(id) {
		this.setState({
			...this.state,
			loading: true,
			errorMessage: ''
		}, () => {
			fetch('http://localhost:3000/record', {
				method: 'DELETE',
				headers: {
					'Content-Type': 'application/json'
				},
				body: JSON.stringify({id: id})
			}).then((response) => {
				if (response.status >= 400) {
                    return {error: 'Sorry, we received a bad server response code: '+String(response.status)+' - '+response.statusText};
                } else {
                    return response.json();
                }
			}).then((json) => {
				if (Object.keys(json).indexOf('error') === -1) {
					this.setState({
						records: json,
						loading: false,
						errorMessage: ''
					}, () => {
						alert("Okay, the item record is now deleted.");
					});
				} else {
					this.setState({
						...this.state,
						errorMessage: json.error
					});
				}
			}).catch((err) => {
				this.setState({
					...this.state,
					errorMessage: 'Sorry, we found a request error.'
				})
			})
		});
	}
	componentDidMount() {
		this.fetchRecords();
	}
	componentDidUpdate(prevProps, prevState, snapshot) {
		if (prevProps !== this.props) {
			this.fetchRecords();
		}
	}
	render() {
		return (
			<table>
				{(this.state.loading) && <div
					style={{
						position: 'absolute',
						width: 'inherit',
						height: 'inherit',
						backgroundColor: 'rgba(150,150,150,.8)',
						display: 'flex',
						justifyContent: 'center',
						alignItems: 'center',
						padding: '10px'
					}}
				>
					<h3>Please wait</h3>
				</div>}
				<tr>
		            <th colspan='3'>Records</th>
		        </tr>
		        <tr>
		            <td>Item</td>
		            <td>Cost</td>
		            <td>Category</td>
  		            <td>Options</td>
		        </tr>
		        {(this.state.errorMessage.length > 0) &&
					<tr>
						<td colspan="3">
							<span
									style={{
										backgroundColor: 'red',
										padding: '10px'
									}}
								><b>hello world</b>
							</span>
						</td>
					</tr>}
		        {this.state.records.map((record) => (
		        	<tr>
		        		<td>{record.name}</td>
		        		<td>{record.cost}</td>
		        		<td>{record.category}</td>
		        		<td>
		        			<button
		        				onClick={(e) => { this.deleteRecord(record.id) }}
		        				style={{
		        					cursor: 'pointer'
		        				}}
		        			>Delete record</button>
		        		</td>
		        	</tr>	
		        ))}
		    </table>
		);
	}
}

export default ExpensesRecordTable;