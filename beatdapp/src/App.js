import React from 'react';
import './App.css';
import ExpensesRecordTable from './ExpensesRecordTable.js';
import ExpenseRecordEntry from './ExpenseRecordEntry.js';

class AppContainer extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      tic: true
    };
    this.renderView = this.renderView.bind(this);
  }

  renderView() {
    this.setState({
      tic: !this.state.tic
    });
  }

  render() {
    return <div className="App">
      <ExpenseRecordEntry resetTable={this.renderView}/>
      <ExpensesRecordTable tic={this.state.tic} />
    </div>
  }
}

function App() {
  return <AppContainer />;
}

export default App;
