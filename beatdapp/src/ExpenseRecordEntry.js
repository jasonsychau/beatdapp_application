import React from 'react'

class ExpensesRecordTable extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			cost: '',
			categories: [],
			loading: false,
			errorMessage: ''
		};
	}
	fetchData() {
		this.setState({
			...this.state,
			loading: true,
			errorMessage: ''
		}, () => {
			fetch('http://localhost:3000/categories').then((response) => {
				if (response.status >= 400) {
                    return {error: 'Sorry, we received a bad server response code: '+String(response.status)+' - '+response.statusText};
                } else {
                    return response.json();
                }
			}).then((json) => {
				if (Object.keys(json).indexOf('error') === -1) {
					this.setState({
						...this.state,
						categories: json,
						loading: false,
						errorMessage: ''
					});
				} else {
					this.setState({
						...this.state,
						errorMessage: json.error
					});
				}
			}).catch((err) => {
				this.setState({
					...this.state,
					errorMessage: 'Sorry, we found a request error.'
				})
			})
		});
	}
	checkCostInput(event) {
		if (/^[0-9]*$/.test(event.target.value)) {
			this.setState({
				...this.state,
				cost: event.target.value
			});
		} else {
			document.getElementById('costInput').value = this.state.cost;
		}
	}
	saveRecord() {
		if (this.state.cost.length === 0) {
			this.setState({
				...this.state,
				errorMessage: 'Sorry, cost is invalid.'
			});
		} else {
			let item = document.getElementById('nameInput').value;
			let category = document.getElementById('categoryInput').value;			
			this.setState({
				...this.state,
				loading: true,
				errorMessage: ''
			}, () => {
				fetch('http://localhost:3000/record', {
					method: 'POST',
					headers: {
						'Content-Type': 'application/json',
						'Access-Control-Allow-Origin': '*'
					},
					body: JSON.stringify({
						name: item,
						cost: this.state.cost,
						category: category
					})
				}).then((response) => {
					if (response.status >= 400) {
	                    return {error: 'Sorry, we received a bad server response code: '+String(response.status)+' - '+response.statusText};
	                } else {
	                    return response.json();
	                }
				}).then((json) => {
					if (Object.keys(json).indexOf('error') === -1) {
						this.setState({
							...this.state,
							categories: json,
							loading: false,
							errorMessage: '',
							item: '',
							cost: ''
						}, () => {
							this.props.resetTable();
						});
					} else {
						this.setState({
							...this.state,
							errorMessage: json.error
						});
					}
				}).catch((err) => {
					this.setState({
						...this.state,
						errorMessage: 'Sorry, we found a request error.'
					})
				})
			});
		}
	}
	componentDidMount() {
		this.fetchData();
	}
	componentDidUpdate(prevProps, prevState, snapshot) {
		if (prevProps !== this.props) {
			this.fetchData();
		}
	}
	render() {
		return (
			<table>
				<tr>
					<th colspan="3">
						<b>Record Entry</b>
					</th>
				</tr>
				{(this.state.loading) ? (
					<tr>
						<td colspan='3'>Please wait</td>
					</tr>
				) : (
					<React.Fragment>
						<React.Fragment>
							{(this.state.errorMessage.length > 0) && <tr>
								<td colspan="3">{this.state.errorMessage}</td>
							</tr>}
						</React.Fragment>
						<tr>
							<td><b>Item name</b></td>
							<td><b>Cost</b></td>
							<td><b>Category</b></td>
						</tr>
						<tr>
							<td><input id='nameInput' type='text' /></td>
							<td><input
								id='costInput'
								type='text'
								style={{textAlign:'right'}}
								onChange={(e) => { this.checkCostInput(e); }} /></td>
							<td><select id='categoryInput' style={{cursor:'pointer'}}>
								{this.state.categories.map((category, index) => <option value={category.id}>{category.name}</option>)}
							</select></td>
						</tr>
						<tr>
							<td colspan="3">
								<button
									onClick={(e) => { this.saveRecord(); }}
									style={{
										width:'100%',
										cursor:'pointer'
									}}
								>Save</button>
							</td>
						</tr>
					</React.Fragment>
				)}
			</table>
		);
	}
}

export default ExpensesRecordTable;